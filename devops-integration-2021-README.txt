-----------------
Pre-requisitos
-----------------

-Tener Habilitado Hyper-V en Windows 10
-Tener Instalado la ultima version de Oracle Virtual Box
-Tener Instalado la ultima version de Vagrant
-Tener un usuario en https://gitlab.com/
-Tener acceso al proyecto https://gitlab.com/devops-integration-2021
 si no tienen enviar una solicitud a wroquem@gmail.com para brindarles el acceso respectivo.

-------------------------------
Pasos a Ejecutar en PC/Laptop
-------------------------------

Crear el siguiente folder "opt" en la unidad de disco que dispongan de espacio ya sea C, D, E o el que elijan
para este ejemplo usaremos D
Crer carpeta opt
D:\opt
dentro de esta carpeta descargar el siguiente archivo https://gitlab.com/devops-integration-2021/project-info/-/raw/master/devops-integration-2021.zip, luego extraer aqui
tendran un grupo de carpetas como estas con las siguientes rutas:
D:/opt/devops-integration-2021
D:/opt/devops-integration-2021/container/docker-project
D:/opt/devops-integration-2021/infrastructure-as-code/vagrant-project
D:/opt/devops-integration-2021/pipeline-as-code/jenkins-project

Luego ingresan a esta carpeta 
D:/opt/devops-integration-2021/infrastructure-as-code/vagrant-project
abren un consola de comando y ejecutan el siguiente comando git
git clone https://gitlab.com/devops-integration-2021/infrastructure-as-code/vagrant-project/centos7-devops.git
va descargar el proyecto centos7-devops ahora ingresan a este proyecto 
D:/opt/devops-integration-2021/infrastructure-as-code/vagrant-project


-------------------------------
Pasos a Ejecutar en Vagrant/Linux
-------------------------------
ahora ejecutan el siguiente comando vagrant
vagrant up
Luego cuando termine de descargar y configurar ingresan a la vm con el siguiente comando
vagrant ssh
un vez dentro les aparecer este mensaje [vagrant@app ~]$
ejecutar el siguiente comando para ser root del Centos7 OS
sudo -s
luego mostrara este mensje [root@app vagrant]#   ahora son root
ahora debemos instalar el servicio de zip y unzip en el OS con el siguiente comando
yum -y install zip unzip
ir a la siguiente carpeta con este comando
cd /opt
en esa carpeta se debe de descargar el siguiente archivo https://gitlab.com/devops-integration-2021/project-info/-/raw/master/devops-integration-2021.zip y extrar con el siguiente comando
wget https://gitlab.com/devops-integration-2021/project-info/-/raw/master/devops-integration-2021.zip
un vez descargado se debe de extraer o descomprimir con el siguiente comando
unzip devops-integration-2021.zip
ir a la siguiente carpeta con este comando
cd devops-integration-2021/
luego ejecutar este comando para saber que debemos estar en esta carperta
pwd
te va mostrar este mensaje /opt/devops-integration-2021
ingresar a esta carpeta con el siguiente comando
cd /opt/devops-integration-2021/container/docker-project/
Ahora debemos con git debemos descargar estos proyectos con sus respectivo comando git
cuando descargen gitlab les va pedir sus credenciales

git clone https://gitlab.com/devops-integration-2021/container/docker-project/mysql5.6.git
git clone https://gitlab.com/devops-integration-2021/container/docker-project/mysql5.7.git
git clone https://gitlab.com/devops-integration-2021/container/docker-project/mysql8.0.git
git clone https://gitlab.com/devops-integration-2021/container/docker-project/tomcat9-openjdk8-centos7.git
git clone https://gitlab.com/devops-integration-2021/container/docker-project/jenkins.lts.git
git clone https://gitlab.com/devops-integration-2021/container/docker-project/sonarqube8.9.0-developer.git
git clone https://gitlab.com/devops-integration-2021/container/docker-project/nexus-oss2.14.git

luego ejecutar el siguiente comando y debe mostrar los proyectos descargados
ls -l
----------------------------------------------------
drwxr-xr-x. 3 root root 60 May 13 20:34 jenkins.lts
drwxr-xr-x. 3 root root 36 May 13 20:21 mysql5.6
drwxr-xr-x. 3 root root 36 May 13 20:30 mysql5.7
drwxr-xr-x. 3 root root 36 May 13 20:29 mysql8.0
drwxr-xr-x. 3 root root 44 May 13 20:47 nexus-oss2.14
drwxr-xr-x. 3 root root 44 May 13 20:47 sonarqube8.9.0-developer
drwxr-xr-x. 3 root root 36 May 13 20:34 tomcat9-openjdk8-centos7

De ahora en adelante solo vamos a utilizar solo comandos docker para construir nuestras propias imagenes, gestionar y ejecutar nuestros propios contenedores

Ingresar a la siguiente carpeta
cd /opt/devops-integration-2021/container/docker-project/mysql5.6
Construyendo una Imagen, ejecutar el siguiente comando
docker build -t mysql:5.6 .

Ingresar a la siguiente carpeta
cd /opt/devops-integration-2021/container/docker-project/mysql5.7
Construyendo una Imagen, ejecutar el siguiente comando
docker build -t mysql:5.7 .

Ingresar a la siguiente carpeta
cd /opt/devops-integration-2021/container/docker-project/mysql8.0
Construyendo una Imagen, ejecutar el siguiente comando
docker build -t mysql:8.0 .

Ingresar a la siguiente carpeta
cd /opt/devops-integration-2021/container/docker-project/mysql5.6-taller-monolithic
Construyendo una Imagen, ejecutar el siguiente comando
docker build -t mysql5.6:taller-monolithic .

Ingresar a la siguiente carpeta
cd /opt/devops-integration-2021/container/docker-project/tomcat9-openjdk8-centos7
Construyendo una Imagen, ejecutar el siguiente comando
docker build -t tomcat9:openjdk8-centos7 .

Levantar imagenes
Ejecutar una Imagen, ejecutar el siguiente comando
docker run -it -d -p 3310:3306 -e MYSQL_ROOT_PASSWORD=root --name mysql56-dev mysql:5.6 mysqld --lower_case_table_names=1
docker run -it -d -p 3900:3306 -e MYSQL_ROOT_PASSWORD=root --name mysql56-taller-monolithic mysql5.6:taller-monolithic mysqld --lower_case_table_names=1
docker run -it -d -p 9900:8080 --name tomcat9-dev tomcat9:openjdk8-centos7

Ir la carpeta
cd /opt
Crear carpeta
mkdir app
Descargar codigo
git clone https://gitlab.com/devops-integration-2021/source-code/java-project/app-talleres-monolithic.git





